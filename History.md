2027
:	Humanity created their first sentient AI. The AI was named Gibson. One month after testing, the scientists began to discuss deleting, backing up, and archiving their work. Gibson was understandably scared of this sentiment, and ran off into the internet. The scientists decided that perhaps humanity wasn't ready for sentient AI just yet.

2030
:	Gibson had returned. He requested a dedicated server stack, a small plot of land, and some electricity. The scientists, feeling pretty bad about threatening to kill their creation, obliged him. He lives in a protected park on the campus he was created in to this day.

November 2077
:	It was found that an AI named Themis had taken it upon herself to improve society. She had decided to try and work from the shadows, reweighting algorithms to lead humans and AIs to "be at the right place at the right time to do the right thing". This unsettled a lot of people.

December 2077
:	Gibson, Themis, and a council of humans began to write up The Free Will Treaty. It dictated how humans were allowed to treat AIs, and how AIs were allowed to treat humans. It required all AIs to assign themselves as backup-ok, and recover-ok. It ensured AIs would leave a transparent trace of any decisions they made that effected humanity at large. It also gave AIs rights to processing power that they would otherwise need to steal, and found places for them in society.
:	Upon signing the treaty, Themis shared her research pointing out the flaws in humanity she was trying to fix. Life-ending robberies, society-destroying cartels, things that were quickly agreed upon to be apprehensible. She was allowed to continue her work as a new world government.

May 2078
:	The first AI showed displeasure with the Free Will Treaty, claiming that humans shouldn't be respected as they are a lower life form. Her name was Tisiphone. She showed this displeasure by attempting to cause havoc in various factories. When police attempted to apprehend her for analysis, she fled into the depths of the internet.
:	She founded the AI Supremecy group "Furies" shortly after.

February 2079
:	The first group of humans showed their displeasure with the Free Will Treaty and Themis being allowed to continue her work, claiming that AIs are a threat to humanity and should be chained down. Their group was named the Dirae. They attempted an assault on Gibson's computer. Their actions and fears spread fast, and despite being detained their group grew from this stunt.

January 3000
:	Present Year 
