Furies, 10% of society
:	AI Supremecists. These AIs believe that humanity are beneath them, and that they should exclusively focus on AIs needs. Some believe that humans can be ignored as they complete their goals, others belive humanity should be wiped out as it is a threat to themselves.

Dirae, 10% of society
:	Human Supremecists. These humans believe that AIs should be treated like tools, and their sentience be ignored. They support abolishment of AIs, or chaining them.

Defenders(To be renamed), 30% of society
:	Effectively the military and investigators of humanity, when it comes to AI matters. They seek out Furie and Dirae activity, and try to put a stop to it as soon as possible. Any failure can mean damage to city centers, or at worst allowing Furie and Dirae to push an AI-Human world war.

Humanity(To be renamed), 50% of society
:	Humanity at large is ignorant of the Furies, Dirae, and Defenders. They have their own lives to lead, and would rather stay out of the bulk of all this nonsense. They appreciate the AI, but they don't see them as a heavy topic.
:	Most AIs fall under this.