This is a collection of supplemental notes for the Megaman BattleNetwork fangame "[NetBattlers](https://drive.google.com/open?id=1zkllTPQnEgDJu6Q5MGmOSYMEIDqKPoiH)"

It introduces a new setting seperate from the default BattleNetwork setting, focused on transhumanism and strays away from the base game's principles.

Instead, it focuses on the following principals:

- Transhumanism
- The friendship between humanity and a digital life form created by them
- The hatred between humanity and a digital life form created by them
- The horrors and benefits of creating a race that is more intelligent than you
- The concepts of the needs of a digital race
- A fight against hatred and entropy

These documents can be read in any order like an encyclopedia.
